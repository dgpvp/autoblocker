package com.dgpvp.minereset.autoblocker.listeners;

import java.util.Collection;
import java.util.Random;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.dgpvp.minereset.autoblocker.AutoBlocker;
import com.dgpvp.minereset.autoblocker.runnables.AsyncAutoBlock;

public class OnBlockBreak implements Listener {

	private AutoBlocker autoBlocker;
	private AsyncAutoBlock blocker;

	public OnBlockBreak(AutoBlocker autoBlocker) {
		this.autoBlocker = autoBlocker;
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled=true)
	public void onBlockBreak(BlockBreakEvent event) {

		if (!autoBlocker.getWorldGuard().canBuild(event.getPlayer(), event.getBlock())) {
			return;
		}
		
		if (event.isCancelled()) {
			return;
		}
		
		if (!event.getPlayer().getGameMode().equals(GameMode.SURVIVAL)) {
			return;
		}
		
		String blockName = event.getBlock().getType().name();
		
		Block block = event.getBlock();
		
		Collection<ItemStack> drops = event.getBlock().getDrops();
		
		event.setCancelled(true);
		
		// If their inventory is full the block is deleted.
		if (!(event.getPlayer().getInventory().firstEmpty() == -1)) {
			
			if (event.getPlayer().getItemInHand().getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1) {
				
				//event.getPlayer().sendMessage("silk");
					
				ItemStack item = new ItemStack(block.getState().getType());
				item.setDurability(block.getData());				
				event.getPlayer().getInventory().addItem(item);
				
				//event.getPlayer().sendMessage("touch");
				
				event.getBlock().setType(Material.AIR);
				
			} else {
				event.getBlock().setType(Material.AIR);
				
				// Automatically put the block in their inventory.
				for (ItemStack drop : drops) {
					if (this.autoBlocker.getConfig().getStringList("affected_by_fortune").contains(blockName)) {
						drop.setAmount(this.getDropCount(event.getPlayer().getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS), new Random()));
					}
				
					event.getPlayer().getInventory().addItem(drop);
				}
			}
		}
		
		// AutoBlock
		blocker = new AsyncAutoBlock(autoBlocker, event.getPlayer());
		blocker.runTaskAsynchronously(autoBlocker);
	}
	
	public int getDropCount(int i, Random random) {
        int j = random.nextInt(i + 2) - 1;
 
        if (j < 0) {
            j = 0;
        }
 
        return (j + 1);
    }
}
