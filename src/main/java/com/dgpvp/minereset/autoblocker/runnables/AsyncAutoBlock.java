package com.dgpvp.minereset.autoblocker.runnables;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.minereset.autoblocker.AutoBlocker;

public class AsyncAutoBlock extends BukkitRunnable {

	Player player;
	private AutoBlocker autoBlocker;
	
	public AsyncAutoBlock(AutoBlocker autoBlocker, Player player) {
		this.autoBlocker = autoBlocker;
		this.player = player;
	}
	
	@Override
	public void run() {
		
		Inventory inv = player.getPlayer().getInventory();
		ItemStack[] items = inv.getContents();
		int itemCount = 0;
		for (String item : autoBlocker.getConfig().getStringList("items")) {
			itemCount = 0;
			for (ItemStack stack : items) {
				if (stack != null) {
					if ((item.equalsIgnoreCase("gold"))
							|| (item.equalsIgnoreCase("iron"))) {
						if (stack.getType() == Material.getMaterial(item
								+ "_INGOT")) {
							itemCount += stack.getAmount();
							inv.remove(stack);
						}
					} else if (stack.getType() == Material.getMaterial(item)) {
						itemCount += stack.getAmount();
						inv.remove(stack);
					}
				}
			}
			
			if (inv.firstEmpty() != -1) {
				if (itemCount / 9 > 0) {
					inv.addItem(new ItemStack[] { new ItemStack(Material
							.getMaterial(item + "_BLOCK"), itemCount / 9) });
				}
				if ((inv.firstEmpty() != -1) && (itemCount % 9 > 0)) {
					if ((item.equalsIgnoreCase("gold"))
							|| (item.equalsIgnoreCase("iron"))) {
						inv.addItem(new ItemStack[] { new ItemStack(Material
								.getMaterial(item + "_INGOT"), itemCount % 9) });
					} else {
						inv.addItem(new ItemStack[] { new ItemStack(Material
								.getMaterial(item), itemCount % 9) });
					}
				}
			}
		}
	}	
	
	
}
