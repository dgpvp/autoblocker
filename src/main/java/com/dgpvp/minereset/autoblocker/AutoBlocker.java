package com.dgpvp.minereset.autoblocker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.dgpvp.minereset.autoblocker.listeners.OnBlockBreak;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class AutoBlocker extends JavaPlugin {

	private HashMap<String, String> itemsBlock = new HashMap<String, String>(); // String 1 = item, String 2 = block recieved with 9 of item
	
	@Override
	public void onEnable() {
		this.loadBlocks();
		this.saveDefaultConfig();
		
		PluginManager pm = this.getServer().getPluginManager();
		
		pm.registerEvents(new OnBlockBreak(this), this);
	}
	
	public Material getItemsBlock(String item) {
		Material block = null;
		
		block = Material.getMaterial(itemsBlock.get(item));
		
		return block;
	}
	
	public List<Material> getItemsToStack() {
		List<Material> list = new ArrayList<Material>();
		
		for (Entry<String, String> set : this.itemsBlock.entrySet()) {
			list.add(Material.getMaterial(set.getKey().toUpperCase()));
		}
		
		return list;
	}
	
	private void loadBlocks() {
		
		for (String itemSet : this.getConfig().getStringList("blocks")) {
			String item, block;
			
			String[] itemBlock = itemSet.split(",");
			
			item = itemBlock[0];
			
			block = itemBlock[1];
			
			this.itemsBlock.put(item, block);
		}
		
	}
	
	public WorldGuardPlugin getWorldGuard() {
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	
}
